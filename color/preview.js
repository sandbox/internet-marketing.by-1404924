
(function ($) {
  Drupal.color = {
    logoChanged: false,
    callback: function(context, settings, form, farb, height, width) {
      // Change the logo to be the real one.
      if (!this.logoChanged) {
        $('#preview #preview-logo img').attr('src', Drupal.settings.color.logo);
        this.logoChanged = true;
      }
      // Remove the logo if the setting is toggled off. 
      if (Drupal.settings.color.logo == null) {
        $('div').remove('#preview-logo');
      }

      // Text and links preview.
      $('#preview #preview-main h2, #preview #preview-cont', form).css('color', $('#palette input[name="palette[text]"]', form).val());
      $('#preview #preview-main a', form).css('color', $('#palette input[name="palette[link]"]', form).val());
      
      $('#preview #preview-sidebar-right a:hover', form).css('color', $('#palette input[name="palette[link]"]', form).val());

      // Main menu links
      $('#preview #preview-sidebar #preview-block', form).css('background-color', $('#palette input[name="palette[link-right]"]', form).val());
      $('#preview #preview-sidebar #preview-block', form).css('border-color', $('#palette input[name="palette[link-left]"]', form).val());

      // CSS3 Gradients.
      var gradient_start = $('#palette input[name="palette[top]"]', form).val();
      var gradient_end = $('#palette input[name="palette[bottom]"]', form).val();

      $('#preview #preview-header-bg', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      $('#preview #preview-block-title', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      
      var gradient_start = $('#palette input[name="palette[link-right]"]', form).val();
      var gradient_end = $('#palette input[name="palette[link-left]"]', form).val();
      
      $('#preview #preview-main-menu li a:hover', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, left top, right top, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      $('#preview #preview-day, #preview #preview-block-title-gradient', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      $('#preview #preview-main-menu .active', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, 0% 0%, 100% 0%, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      $('#preview #preview-comment-number', form).attr('style', "background-color: " + gradient_start + "; background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(" + gradient_start + "), to(" + gradient_end + ")); background-image: -moz-linear-gradient(-90deg, " + gradient_start + ", " + gradient_end + ");");
      
      var gradient = $('#palette input[name="palette[comment-odd]"]', form).val();
      
      $('#preview #preview-comment-odd', form).attr('style', "background-color: " + gradient + "; background-image: -webkit-gradient(linear, left top, right top, from(" + gradient + "), to(#fff)); background-image: -moz-linear-gradient(-90deg, " + gradient + ", #fff);");
      
      var gradient = $('#palette input[name="palette[comment-even]"]', form).val();
      
      $('#preview #preview-comment-even', form).attr('style', "background-color: " + gradient + "; background-image: -webkit-gradient(linear, left top, right top, from(" + gradient + "), to(#fff)); background-image: -moz-linear-gradient(-90deg, " + gradient + ", #fff);");
      
      var gradient = $('#palette input[name="palette[admin comment]"]', form).val();
      
      $('#preview #preview-comment-admin', form).attr('style', "background-color: " + gradient + "; background-image: -webkit-gradient(linear, left top, right top, from(" + gradient + "), to(#fff)); background-image: -moz-linear-gradient(-90deg, " + gradient + ", #fff);");
      
      $('#preview #preview-site-name', form).css('color', $('#palette input[name="palette[site-name]"]', form).val());
    }
  };
})(jQuery);
