﻿<?php

// Put the logo path into JavaScript for the live preview.
drupal_add_js(array('color' => array('logo' => theme_get_setting('logo', 'im_director'))), 'setting');

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'top' => t('Header top'),
    'bottom' => t('Header bottom'),
    'link-left' => t('Link left'),
    'link-right' => t('Link right'),
    'comment-odd' => t('Comment odd'),
    'comment-even' => t('Comment even'),
    'admin-comment' => t('Admin comment'),
    'site-name' => t('Site name'),
    'text' => t('Text color'),
    'link' => t('Link color'),
  ),
  // Pre-defined color schemes.
    
  'schemes' => array( 
    'default' => array(
      'title' => t('Gray (default)'),
      'colors' => array(
        'top' => '#9c9c9c',
        'bottom' => '#717171',
        'link-left' => '#1177a2',
        'link-right' => '#59b9e1',
        'comment-odd' => '#fafafa',
        'comment-even' => '#dff6ff',
        'admin-comment' => '#fff1c2',
        'site-name' => '#828282',
        'text' => '#3B3B3B',
        'link' => '#259ACB',
      ),
    ),
    'orange' => array(
      'title' => t('Orange'),
      'colors' => array(
        'top' => '#fcb918',
        'bottom' => '#fb8008',
        'link-left' => '#dd6005',
        'link-right' => '#ff9813',
        'comment-odd' => '#fafafa',
        'comment-even' => '#fff1c2',
        'admin-comment' => '#dff6ff',
        'site-name' => '#828282',
        'text' => '#3b3b3b',
        'link' => '#259ACB',
      ),
    ),
    'blue' => array(
      'title' => t('Blue - gray'),
      'colors' => array(
        'top' => '#83c9fc',
        'bottom' => '#1e76ba',
        'link-left' => '#707070',
        'link-right' => '#b5b5b5',
        'comment-odd' => '#fafafa',
        'comment-even' => '#dff6ff',
        'admin-comment' => '#fff1c2',
        'site-name' => '#828282',
        'text' => '#3b3b3b',
        'link' => '#259ACB',
      ),
    ),
    'green' => array(
      'title' => t('Green - orange'),
      'colors' => array(
        'top' => '#a2eb44',
        'bottom' => '#5ca21e',
        'link-left' => '#e29c14',
        'link-right' => '#f6d628',
        'comment-odd' => '#e8ffb7',
        'comment-even' => '#fff1c2',
        'admin-comment' => '#dff6ff',
        'site-name' => '#828282',
        'text' => '#3b3b3b',
        'link' => '#259ACB',
      ),
    ),
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.png',
  ),

  // Gradient definitions.
  'gradients' => array(
    array(
      // (x, y, width, height).
      'dimension' => array(0, 0, 0, 0),
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => array('top', 'bottom'),
    ),
  ),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
