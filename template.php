<?php
/**
 * Implements template_preprocess_html().
 */
function im_director_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Implements template_preprocess_page().
 */
function im_director_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  drupal_add_library ('system', 'ui.widget');
}

/**
 * Implements hook_form_alter().
 */
function im_director_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {
    unset($form['search_block_form']['#title']);
    $form['actions']['submit']['#value'] = '';
    $form['search_block_form']['#default_value'] = t('Look for what?');
    $form['search_block_form']['#attributes'] = array(
      'onclick' => "if (this.value == '{$form['search_block_form']['#default_value']}') {this.value = ''}",
      'onblur' => "if (this.value.length == 0) {this.value = '{$form['search_block_form']['#default_value']}'}"
    );
  }
  if ($form_id == 'search_form') {
    unset($form['basic']['keys']['#title']);
    $form['basic']['submit']['#value'] = '';
    $form['basic']['keys']['#default_value'] = t('Look for what?');
    $form['basic']['keys']['#attributes'] = array(
      'onclick' => "if (this.value == '{$form['basic']['keys']['#default_value']}') {this.value = ''}",
      'onblur' => "if (this.value.length == 0) {this.value = '{$form['basic']['keys']['#default_value']}'}"
    );
  }
}

/**
 * Implements hook_block_view_alter().
 */
function im_director_block_view_alter(&$delta, $block) {
  // Override links for Poll
  if ($block->module == 'poll'){
      $node = $delta['content']['poll_view_voting']['#node'];
      $links = array();
      $links[] = array(
        'title' => t('Add new comment'), 
        'href' => 'node/' . $node->nid, 
        'attributes' => array('title' => t('Add new comment')));
      $links[] = array(
        'title' => t('Older polls'), 
        'href' => 'poll', 'attributes' => array(
          'title' => t('View the list of polls on this site.')));
      $delta['content']['links']['#links'] = $links;
    }
}

/**
 * Override or insert variables into the node template.
 */
function im_director_preprocess_node(&$varaibles) {
  // Add link to author's blog for other node types
  if ($varaibles['type'] != 'blog') {
    $links['blog_usernames_blog'] = array(
      'title' => t("!username's blog", array(
        '!username' => format_username($varaibles['elements']['#node']))),
      'href' => 'blog/' . $varaibles['elements']['#node']->uid,
      'attributes' => array(
        'title' => t("Read !username's latest blog entries.", array(
          '!username' => format_username($varaibles['elements']['#node'])))),
    );
    $varaibles['content']['links']['blog'] = array(
      '#theme' => 'links__node__blog',
      '#links' => $links,
      '#attributes' => array('class' => array('links', 'inline')),
    );
  }
}

/**
 * Override or insert variables into the block template.
 */
function im_director_preprocess_block(&$variables) {
  // In the header and footer regions visually hide block titles.
  if ($variables['block']->region == 'header' || $variables['block']->region == 'footer') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Override or insert variables into the comment template.
 */
function im_director_preprocess_comment(&$variables) {
  // Add new variable
  $variables['number'] = $variables['id'];

  $comment = $variables['elements']['#comment'];

  // Set status to a string representation of comment->status.
  if (isset($comment->in_preview)) {
    $variables['status'] = 'comment-preview';
  }
  else {
    $variables['status'] = ($comment->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : 'comment-published';
  }

  // Published class is not needed. It is either 'comment-preview' or 'comment-unpublished'.
  if ($variables['status'] != 'comment-published') {
    $variables['classes_array'][] = $variables['status'];
  }

  // Add specific class if comment was posted by admin
  if ($comment->uid == 1) {
    $variables['classes_array'][] = 'admin-comment';
  }
}

/**
 * Implements theme_field__field_type().
 */
function im_director_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label">' . $variables['label'] . ': </div>';
  }

  // Render the items.
  $number = count($variables['items']);
  $divider = ', ';
  $i = 1;

  foreach ($variables['items'] as $delta => $item) {
    if ($number == $i) {
      $divider = '';
    }
    $output .= '<div class="taxonomy-term-reference taxonomy-term-reference-' . $delta . '"';
    $output .= $variables['item_attributes'][$delta] . '>' . drupal_render($item) . $divider.'</div>';
    $i++;
  }

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '">' . $output . '</div>';
  return $output;
}

/**
 * Override default theme_filter_tips_more_info().
 */
function im_director_filter_tips_more_info() {
  return '';
}

/**
 * Implements hook_preprocess_username().
 */
function im_director_preprocess_username(&$variables) {
  unset($variables['extra']);
}

/**
 * Implements theme_feed_icon().
 */
function im_director_feed_icon($variables) {
  $text = t('Subscribe to @feed-title', array('@feed-title' => $variables['title']));
  return l( '', $variables['url'], array(
    'html' => TRUE, 'attributes' => array(
      'class' => array('feed-icon'), 'title' => $text)));
}
