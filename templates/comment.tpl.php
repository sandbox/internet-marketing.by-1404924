<div class="<?php print $classes; ?> comment-<?php print $zebra; ?> clearfix"<?php print $attributes; ?>>
  <?php print $picture ?>
  <div class="submitted">
    <div class="comment-number">
      <?php  print render($number); ?>
    </div>
    <?php print render($author);?>
  </div>
  <div class="comment-date">
    <?php print (format_date($comment->created, 'custom', 'd.m.Y')); ?>
  </div>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['links']);
      print render($content);
    ?>
    <?php if ($signature): ?>
      <div class="user-signature clearfix">
        <?php print ($signature); ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="links-wrapper">
    <?php print render($content['links']); ?>
  </div>
</div>
