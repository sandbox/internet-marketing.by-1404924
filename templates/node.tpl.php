<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="date-bg">
    <div class="date-wrapper">  
      <div class="date-day">
        <?php print(format_date($created, 'custom', 'd')); ?>
      </div>
      <div class="date-month">
        <?php print(format_date($created, 'custom', 'M')); ?>
      </div>
    </div>
  </div>
  <?php if(!$page): ?>
    <div class="title-wrapper">
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    </div>
  <?php else: ?>
    <div class="title-wrapper">
      <h1<?php print $title_attributes; ?>><?php print $title; ?></h1>
    </div>
  <?php endif; ?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
      $content['field_image']['#label_display']='hidden';
      print render($content);
    ?>
  </div>
  <div class="links-wrapper">
    <?php 
      if (!$page) {
        unset($content['links']['comment']['#links']['comment-comments']);
        unset($content['links']['comment']['#links']['comment-new-comments']);
        print render($content['links']['blog']);
        print render($content['links']['comment']);
        print render($content['links']['node']);
      }
      else {
        print render($content['links']['blog']);
      }
    ?>
  </div>
  <?php print render($content['comments']); ?>
</div>
