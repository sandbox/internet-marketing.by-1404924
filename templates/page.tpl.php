<div id="site_content">  
  <?php echo $feed_icons;?>
  <div id="header">    
    <div class="logo">
      <a href="<?php echo $base_path; ?>" rel="home">
        <img src="<?php echo $logo; ?>" id="logo" />
      </a>
    </div>
    <div id="head-right">
      <div id="site-name-div">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="site-name"><span><?php print $site_name; ?></span></a>
      </div>
      <?php if ($page['header']): ?>
      <?php print render ($page['header']);?>
      <?php endif; ?>
    </div>  
  </div>
  <div id="header-bg">
    <div id="header-mid-bg"></div>
    <div id="header-bot-bg"></div>
    <div id="header-menu-bg"></div>
  </div>  
  <?php if ($page['wrapper_top']): ?>
  <?php print render ($page['wrapper_top']);?>
  <?php endif; ?>
  <div id="content">
    <div id="cont">
      <div id="cont-col">
        <?php print render ($breadcrumb); ?>
        <?php print render ($tabs); ?>
        <?php print render ($page['content']) ?>
        <?php print render ($page['content_bottom']) ?>
      </div>
    </div>
    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-left" class="sidebar">
        <?php print render ($page['sidebar_first']); ?>
      </div>
    <?php endif; ?>
    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-right" class="sidebar">
        <?php print render ($page['sidebar_second']); ?>
      </div>
    <?php endif; ?>
  </div>
  <div id="footer">
    <?php print render ($page['footer']); ?>
  </div>
  <div id="im">
    <a href="http://www.internet-marketing.by" title="<?php print t('Websites creating'); ?>"><?php print t('Websites creating'); ?></a>
  </div>
</div>
