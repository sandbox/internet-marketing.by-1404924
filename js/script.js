
jQuery(function($) {
  $('[type=radio]').wrap("<label class='radio-option radio-unchecked'></label>");
  $('[type=radio]').addClass('invisibly');
  function setupLabel() {
    $('.radio-option.radio-checked').each(function() {
      $(this).removeClass('radio-checked');
      $(this).addClass('radio-unchecked'); 
    });
    $('.radio-option input:checked').each(function() {
      $(this).parent('label').addClass('radio-checked');
      $(this).parent('label').removeClass('radio-unchecked');
    });
  };
  $('.radio-option').click(function() {
    setupLabel();
  });
});
